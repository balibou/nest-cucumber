import { Module } from '@nestjs/common';
import InMemoryProductRepository from '../../domain/stubs/InMemoryProduct.repository';
import { ListProducts } from '../../domain/usecases/ListProducts';
import { ProductController } from './product.controller';

@Module({
  providers: [
    InMemoryProductRepository,
    {
      inject: [InMemoryProductRepository],
      provide: ListProducts,
      useFactory: (productRepository: InMemoryProductRepository) =>
        new ListProducts(productRepository),
    },
  ],
  controllers: [ProductController],
})
export class RestModule {}
