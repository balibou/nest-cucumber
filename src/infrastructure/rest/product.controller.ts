import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { Product } from '../../domain/product/Product';
import { ListProducts } from '../../domain/usecases/ListProducts';

@Controller('/api/product')
export class ProductController {
  constructor(private readonly listProducts: ListProducts) {}

  @Get('/list')
  @HttpCode(HttpStatus.OK)
  async listProduct(): Promise<Product[]> {
    return this.listProducts.execute();
  }
}
