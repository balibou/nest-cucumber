import { Product } from '../product/Product';
import { ProductRepository } from '../product/Product.repository';

export default class InMemoryProductRepository implements ProductRepository {
  private productList = [];

  listProducts() {
    return this.productList;
  }
  addProduct(product: Product) {
    this.productList.push(product);
  }
  clearProductList() {
    this.productList = [];
  }
}
