export class Product {
  constructor(public readonly id: string, public readonly name: string) {
    ['id', 'name'].forEach((key: string) => {
      if ((this[key as keyof Product] as string).trim().length == 0) {
        throw new Error(`Product ${key} must not be blank`);
      }
    });
  }
}
