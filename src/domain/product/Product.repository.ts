import { Product } from './Product';

export interface ProductRepository {
  listProducts: () => Product[];
}
