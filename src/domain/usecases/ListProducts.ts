import { Product } from '../product/Product';
import { ProductRepository } from '../product/product.repository';

export class ListProducts {
  constructor(private readonly productRepository: ProductRepository) {}
  async execute(): Promise<Product[]> {
    return this.productRepository.listProducts();
  }
}
