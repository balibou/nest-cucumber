Feature: List products
  In order to make some errands
  I want to read the list of available products

  Scenario: A customer wants to see the available products
    Given a product
    And a second product
    When the customer looks for the available products
    Then he must see all the available products
