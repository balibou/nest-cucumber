import { Test, TestingModule } from '@nestjs/testing';
import { loadFeature, defineFeature } from 'jest-cucumber';

import { ProductRepository } from '../src/domain/product/Product.repository';
import { Product } from '../src/domain/product/Product';

import InMemoryProductRepository from '../src/domain/stubs/InMemoryProduct.repository';

import { ListProducts } from '../src/domain/usecases/ListProducts';

const feature = loadFeature('./features/listing-products.feature');

defineFeature(feature, (test) => {
  let listProducts: ListProducts;
  let productRepository: ProductRepository;
  let listedProducts: Product[];
  let listedProductsResults: Product[];

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      providers: [
        InMemoryProductRepository,
        {
          inject: [InMemoryProductRepository],
          provide: ListProducts,
          useFactory: (productRepository: InMemoryProductRepository) =>
            new ListProducts(productRepository),
        },
      ],
    }).compile();
    listProducts = await moduleFixture.get<ListProducts>(ListProducts);
    productRepository = await moduleFixture.get<ProductRepository>(
      InMemoryProductRepository,
    );
  });

  beforeEach(() => {
    (productRepository as InMemoryProductRepository).clearProductList();
    listedProducts = [];
    listedProductsResults = [];
  });

  test('A customer wants to see the available products', ({
    given,
    when,
    then,
    and,
  }) => {
    given('a product', async function () {
      const product = new Product('pr1', 'product1');
      listedProducts.push(product);
      (productRepository as InMemoryProductRepository).addProduct(product);
    });
    and('a second product', () => {
      const product = new Product('pr2', 'product2');
      listedProducts.push(product);
      (productRepository as InMemoryProductRepository).addProduct(product);
    });

    when('the customer looks for the available products', async () => {
      listedProductsResults = await listProducts.execute();
    });

    then('he must see all the available products', () => {
      expect(listedProductsResults).toEqual(listedProducts);
    });
  });
});
