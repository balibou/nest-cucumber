import { Product } from '../src/domain/product/Product';

describe('Product', () => {
  it('should not have an empty id', () => {
    const result = () => new Product('', 'product1');
    expect(result).toThrow(new Error('Product id must not be blank'));
  });

  it('should not have an empty name', () => {
    const result = () => new Product('pr1', '');
    expect(result).toThrow(new Error('Product name must not be blank'));
  });
});
