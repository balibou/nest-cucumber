import { INestApplication } from '@nestjs/common';
import { TestingModule, Test } from '@nestjs/testing';
import * as request from 'supertest';

import { Product } from '../src/domain/product/Product';
import { ProductRepository } from '../src/domain/product/Product.repository';
import InMemoryProductRepository from '../src/domain/stubs/InMemoryProduct.repository';

import { RestModule } from '../src/infrastructure/rest/rest.module';

describe('Product controller (e2e)', () => {
  let app: INestApplication;
  let productRepository: ProductRepository;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [RestModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
    productRepository = await moduleFixture.get<ProductRepository>(
      InMemoryProductRepository,
    );
  });

  it('should fetch all products', async () => {
    const product = new Product('pr1', 'product1');
    (productRepository as InMemoryProductRepository).addProduct(product);
    const response: request.Response = await request(app.getHttpServer()).get(
      '/api/product/list',
    );

    expect(response.body).toEqual([product]);
  });
});
